import React from 'react';
import './App.css';
import {Switch , Route , BrowserRouter as Router} from 'react-router-dom';
import Login from './Login/Login';
import Homepage from './Homepage/Homepage';



function App() {
  const [data,setData] = React.useState({
    facebook: {},
    google: {}
  })

  // const _checkEmptyObj = (obj) => {
  //   return Object.entries(obj).length === 0 && obj.constructor === 'object';
  // }

  const _getDataFromLoginComponent = (info) => {
    console.log(info);
    setData({...data,google : info.profileObj})
  }
  const _getDataFromLoginFbComponent = (info) => {
    console.log(info);
    setData({...data,facebook : info})
  }


  
  return (
    <Router className="App">
      <Switch>
          <Route exact path="/"  component={router => <Login {...router} callback={_getDataFromLoginComponent} callback2={_getDataFromLoginFbComponent}/>}>
          </Route>
          <Route exact path="/homepage" component={router => <Homepage {...router} {...data}/>}>
          </Route>
        </Switch>
    </Router>
  );
}

export default App;
