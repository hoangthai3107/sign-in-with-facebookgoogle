import React from "react";
import GoogleLogin from "react-google-login";
import FbLogin from "react-facebook-login";
import "./Login.scss";
import { Row, Col, Container } from "react-bootstrap";

const Login = props => {
  const ResponseGoogle = response => {
    console.log(response);
    props.history.push("/homepage");
    props.callback(response);
  };
  const ResponseFacebook = response => {
    console.log(response);
    props.callback2(response);
    props.history.push("/homepage");
  };

  return (
    <div>
      <Container className="card-login">
        <Row>
          <Col md={{ span: 6, offset: 3 }}>
              <GoogleLogin
                clientId="672961833498-mtvp3vujg447lkv59obfmbnbvte7340k.apps.googleusercontent.com"
                buttonText="Login with Google"
                onSuccess={ResponseGoogle}
                onFailure={ResponseGoogle}
                cookiePolicy={"single_host_origin"}
                className="google"
              />
              <FbLogin
                appId="272300280407233"
                fields="name,email,picture"
                callback={ResponseFacebook}
                cssClass="fb-login"
              />
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Login;
