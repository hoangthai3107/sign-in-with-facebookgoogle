import React from "react";
import { GoogleLogout } from "react-google-login";

const Homepage = props => {
  console.log(props);
  const _checkEmptyObj = obj => {
    return Object.entries(obj).length === 0;
  };

//console.log(props.facebook);
    const ResponseGoogle = (response) => {  
    console.log(response);
    // console.log(props);
    props.history.push('/');
    
  }

  const _renderFb = (
      <div>
          {_checkEmptyObj(props.facebook) ? null : (
              <div>
                  <p>Họ và tên : {props.facebook.name}</p>
                  <p>Email : {props.facebook.email}</p>
                  <img src={props.facebook.picture.data.url} style={{ height: "100px", width: "100px" }}/>
              </div>
          )}
      </div>
  )
  return (
    <div>
      {_checkEmptyObj(props.google) ? null : (
        <div>
          <GoogleLogout
            clientId="672961833498-mtvp3vujg447lkv59obfmbnbvte7340k.apps.googleusercontent.com"
            onLogoutSuccess={ResponseGoogle}
            buttonText="Logout"
          />
          <p>Họ và tên : {props.google.name}</p>
          <p>Email : {props.google.email}</p>
          <img
            src={props.google.imageUrl}
            style={{ height: "100px", width: "100px" }}
          />
        </div>
      )}
      {_renderFb}
    </div>
  );
};

export default Homepage;
